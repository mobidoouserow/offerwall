//
//  OWAppDelegate.m
//  OfferWall
//
//  Created by Paul V. @bis_nival @telegram on 3/15/17
//  Copyright (c) 2014 Bars-i-Lis. All rights reserved.
//

#import "OWAppDelegate.h"



#if HAS_POD(CocoaLumberjack)
    #import <CocoaLumberjack/CocoaLumberjack.h>

    #if HAS_POD(Sidecar)
    #import <Sidecar/CRLMethodLogFormatter.h>
    #endif
#endif

#if HAS_POD(LGSideMenuController)
    #import <LGSideMenuController/LGSideMenuController.h>
    #import <LGSideMenuController/UIViewController+LGSideMenuController.h>
#endif

#import "OWMainViewController.h"
#import "OWMenuViewController.h"
#import "OWSignUpViewController.h"

@interface OWAppDelegate ()

@property (strong) OWMainViewController *mainVC;
@property (strong) OWMenuViewController *menuVC;
@property (strong) OWSignUpViewController *signUp;
@property (strong) UINavigationController *mainNavigationController;

@end

@implementation OWAppDelegate

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initializeLogging];
    [self initSlideControllers];
    return YES;
}

#pragma mark - LGSlide
- (void)initSlideControllers {
    
    if (!_menuVC) {
        _menuVC = [OWMenuViewController storyboardInstance];
    }
    
    if (!_mainVC) {
        _mainVC = [OWMainViewController storyboardInstance];
    }
    
    if (!_mainNavigationController) {
        _mainNavigationController = [[UINavigationController alloc] initWithRootViewController:_mainVC];
    }
    
//    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:_mainNavigationController
//                                                                                           leftViewController:_menuVC
//                                                                                          rightViewController:nil];
//    
//    sideMenuController.leftViewWidth = 330.0f;
//    sideMenuController.leftViewPresentationStyle = LGSideMenuPresentationStyleSlideAbove;
}


#pragma mark Amaro foundation goodies

/**
 Sets up CocoaLumberjack
 */
-(void)initializeLogging
{
    #if HAS_POD(CocoaLumberjack)
        #if HAS_POD(Sidecar)
        CRLMethodLogFormatter *logFormatter = [[CRLMethodLogFormatter alloc] init];
        [[DDASLLogger sharedInstance] setLogFormatter:logFormatter];
        [[DDTTYLogger sharedInstance] setLogFormatter:logFormatter];
        #endif

        // Emulate NSLog behavior for DDLog*
        [DDLog addLogger:[DDASLLogger sharedInstance]];
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
    #endif
}

@end

//
//  main.m
//  OfferWall
//
//  Created by Paul V. @bis_nival @telegram on 3/15/17
//  Copyright (c) 2014 Bars-i-Lis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OWAppDelegate class]));
    }
}

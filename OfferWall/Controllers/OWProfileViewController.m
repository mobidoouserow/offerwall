//
//  OWProfileViewController.m
//  OfferWall
//
//  Created by Pavel Wasilenko on 05/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

@import QuartzCore;

#import "OWProfileViewController.h"

#import "OWBaseView.h"

@interface OWProfileViewController ()

//@property (weak, nonatomic) IBOutlet OWBaseView *photoContainerView;
@end

@implementation OWProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.photoContainerView.cornerRadius = 37.0;
//    self.photoContainerView.borderUIColor = [UIColor avatarBorderColor];
//    self.photoContainerView.borderWidth = 2.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

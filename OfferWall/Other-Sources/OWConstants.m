//
//  OWConstants.m
//  OfferWall
//
//  Created by Paul V. @bis_nival @telegram on 3/15/17
//  Copyright (c) 2014 Bars-i-Lis. All rights reserved.
//

#import "OfferWall-Environment.h"

// Use this file to define the values of the variables declared in the header.
// For data types that aren't compile-time constants (e.g. NSURL), use the
// OWConstantsInitializer function below.

// See OfferWall-Environment.h for macros that are likely applicable in
// this file. TARGETING_{STAGING,PRODUCTION} and IF_STAGING are probably
// the most useful.

// The values here are just examples.

#ifdef TARGETING_STAGING

//NSString * const OWAPIKey = @"StagingKey";

#else

//NSString * const OWAPIKey = @"ProductionKey";

#endif


//NSURL *OWAPIRoot;
void __attribute__((constructor)) OWConstantsInitializer()
{
//    OWAPIRoot = [NSURL URLWithString:IF_STAGING(@"http://myapp.com/api/staging", @"http://myapp.com/api")];
}
//
//  OWConstants.h
//  OfferWall
//
//  Created by Paul V. @bis_nival @telegram on 3/15/17
//  Copyright (c) 2014 Bars-i-Lis. All rights reserved.
//

#import <Foundation/Foundation.h>

// Use this file to declare constants, especially those that change
// between staging and production.

// For example:
//extern NSString * const OWAPIKey;
//extern NSURL * const OWAPIRoot;
//
//  UIColor+OW.m
//  OfferWall
//
//  Created by Pavel Wasilenko on 04/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "UIColor+OW.h"

@implementation UIColor (OW)

+ (UIColor *)colorWithIntRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue
{
    UIColor *color = [UIColor  colorWithRed:(CGFloat)red/255.0 green:(CGFloat)green/255.0 blue:(CGFloat)blue/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)statusBarBackgroundColor
{
    UIColor *color = [UIColor colorWithWhite:20.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuSeparatorColor
{
    UIColor *color = [UIColor colorWithWhite:20.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuLogoGradientStartColor
{
    UIColor *color = [UIColor colorWithWhite:66.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuLogoGradientEndColor
{
    UIColor *color = [UIColor colorWithWhite:54.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuBackgroundColor
{
    UIColor *color = [UIColor colorWithWhite:54.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuSelectedPunctColor
{
    UIColor *color = [UIColor colorWithIntRed:133
                                        green:118
                                         blue:53];
    
    return color;
}

+ (UIColor *)avatarBorderColor
{
    UIColor *color = [UIColor colorWithIntRed:255
                                        green:61
                                         blue:0];
    
    return color;
}

@end

//
//  UIColor+OW.h
//  OfferWall
//
//  Created by Pavel Wasilenko on 04/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (OW)

+ (UIColor *)colorWithIntRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue;

+ (UIColor *)statusBarBackgroundColor;

+ (UIColor *)menuSeparatorColor;

+ (UIColor *)menuLogoGradientStartColor;

+ (UIColor *)menuLogoGradientEndColor;

+ (UIColor *)menuBackgroundColor;

+ (UIColor *)menuSelectedPunctColor;

+ (UIColor *)avatarBorderColor;

@end

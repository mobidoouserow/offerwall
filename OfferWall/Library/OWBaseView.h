//
//  OWBaseView.h
//  OfferWall
//
//  Created by Pavel Wasilenko on 05/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import <UIKit/UIKit.h>

@import QuartzCore;

//IB_DESIGNABLE
@interface OWBaseView : UIView

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic, nullable) IBInspectable UIColor *borderUIColor;

@end

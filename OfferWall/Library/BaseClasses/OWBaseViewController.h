//
//  OWBaseViewController.h
//  OfferWall
//
//  Created by Pavel Wasilenko on 04/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OWBaseViewController : UIViewController

+ (instancetype)storyboardInstance;

@end

//
//  OWBaseViewController.m
//  OfferWall
//
//  Created by Pavel Wasilenko on 04/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "OWBaseViewController.h"

@interface OWBaseViewController ()

@end

@implementation OWBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

+ (instancetype)storyboardInstance {
    UIStoryboard *story = [UIStoryboard storyboardWithName:NSStringFromClass([self class]) bundle:nil];
    
    id controller;
    
    controller = [story instantiateInitialViewController];
    
    if (controller && [controller isKindOfClass:[self class]]) {
        return controller;
    }
    
    controller = [story instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    
    if (controller && [controller isKindOfClass:[self class]]) {
        return controller;
    } else {
        return nil;
    }
}

@end
